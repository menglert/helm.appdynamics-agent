# AppDynamics Agent Helm Chart

[AppDynamics](https://www.appdynamics.com) is an Application Performance Monitoring platform.

## Prerequisites

-   Kubernetes 1.9+

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ git clone https://gitlab.com/menglert/helm.appdynamics-agent.git
$ cd helm.appdynamics-agent
$ helm install . --name my-release \
    --set agentCommon.controllerHost=<tenant>.saas.appdynamics.com \
    --set agentCommon.controllerPort=<port> \
    --set agentCommon.controllerSsl=<true/false> \
    --set agentCommon.accountName=<tenant> \
    --set agentCommon.accessKey=<access-key> \
```

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

The following table lists the configurable parameters of the AppDynamics chart and their default values.

| Parameter                           | Description                                                                                                            | Default                                  |
| ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- |
| `daemonset.enabled`                 | If true, enable daemonset.                                                                                             | `true`                                   |
| `daemonset.nodeSelector`            | Pod node selector for daemonset.                                                                                       | `{}`                                     |
| `daemonset.resources.requests.cpu`  | CPU resource requests for daemonset.                                                                                   |                                          |
| `daemonset.resources.limits.cpu`    | CPU resource limits for daemonset.                                                                                     |                                          |
| `agentCommon.image.repository`      | The image repository to pull from.                                                                                     | `docker.io/michaelenglert/appd-agent`    |
| `agentCommon.image.repository`      | The image tag to pull.                                                                                                 | `latest`                                 |
| `agentCommon.image.pullPolicy`      | Image pull policy.                                                                                                     | `IfNotPresent`                           |
| `agentCommon.controllerHost`        | AppDynamics Controller Host the Agent should connect to.                                                               | `<tenant>.saas.appdynamics.com`          |
| `agentCommon.controllerPort`        | AppDynamics Controller Port the Agent should connect to.                                                               | `443`                                    |
| `agentCommon.controllerSsl`         | Set to true of the communication is via SSL. Set to false if no SSL should be used.                                    | `true`                                   |
| `agentCommon.accountName`           | The Account Name that should be used to connect to the AppDynamics Controller.                                         | `<tenant>`                               |
| `agentCommon.accessKey`             | The Account Name that should be used to connect to the AppDynamics Controller.                                         | `null`                                   |
| `agentCommon.stdoutLogging`         | Controls if the Agent Log Files are written to Standard Out or not.                                                    | `true`                                   |
| `machine.simEnabled`                | If the Server Visibility Feature is enabled or not.                                                                    | `true`                                   |
| `machine.dockerEnabled`             | If the Docker Visibility Feature is enabled or not.                                                                    | `true`                                   |
| `analytics.eventEndpoint`           | When Analytics (Business iQ) should be used the Agent needs to know where to send its' data.                           | `https://analytics.api.appdynamics.com/` |
| `analytics.globalAccountName`       | When Analytics (Business iQ) should be used the Agent needs to know to which Account to send its' data.                | `null`                                   |
| `extraVolumes`, `extraVolumeMounts` | Additional volumes and mounts, for example to provide other configuration files.                                       |                                          |
| `resources.requests.cpu`            | CPU resource requests.                                                                                                 |                                          |
| `resources.limits.cpu`              | CPU resource limits.                                                                                                   |                                          |
| `resources.requests.memory`         | Memory resource requests.                                                                                              |                                          |
| `resources.limits.memory`           | Memory resource limits.                                                                                                |                                          |
| `rbac.create`                       | If true, create & use RBAC resources.                                                                                  | `true`                                   |
| `serviceAccount.create`             | If true, create & use ServiceAccount.                                                                                  | `true`                                   |
| `serviceAccount.name`               | The name of the ServiceAccount to use. If not set and create is true, a name is generated using the fullname template. |                                          |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```bash
$ helm install . --name my-release \
    --set rbac.create=true \
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```bash
$ helm install . --name my-release -f values.yaml
```

> **Tip**: You can use the default [values.yaml](values.yaml)